FROM node:7
COPY package*.json ./
RUN npm install
CMD node index.js
EXPOSE 3000
